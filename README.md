## ChordSeq ##

ChordSeq offers a simple way to create MIDI rhythms and riffs by defining a velocity sequence synchronized on the Live tempo. Different modes allow playing with the device in many ways with incoming MIDI notes.

## Multi Fx ##

MultiFX embeds 4 effects in one. It was designed as a simple solution to add effects to a synth (as a GeissEnveloper) like in most hardware devices. The signal is sent in a delay line with feedback, a simple reverb module and goes in parallel into a Chorus and an Auto Pan. Some of the modules can be synchronized with the Live tempo. MultiFX is a simple FX box for all your plugins.


> **Important note:** For Live 8 compatible devices, see [Archive](http://forumnet.ircam.fr/shop/en/archived.php?id_product=55&archived=1).